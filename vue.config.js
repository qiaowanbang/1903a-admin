const path = require("path");
module.exports = {
  configureWebpack: {
    resolve: {
      // 别名
      alias: {
        "@": path.join(__dirname, "src"),
      },
    },
  },
};
