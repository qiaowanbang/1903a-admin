import { Menu, Layout } from "ant-design-vue";
console.log(Layout);
console.log(Menu);
export default {
  install(Vue) {
    Vue.component(Layout.name, Layout);
    const { Content, Footer, Header, Sider } = Layout;
    Vue.component(Content.name, Content);
    Vue.component(Footer.name, Footer);
    Vue.component(Header.name, Header);
    Vue.component(Sider.name, Sider);
    Vue.component(Menu.name, Menu);
    const { Item, ItemGroup, SubMenu, Divider } = Menu;
    Vue.component(Divider.name, Divider);
    Vue.component(Item.name, Item);
    Vue.component(ItemGroup.name, ItemGroup);
    Vue.component(SubMenu.name, SubMenu);
  },
};
