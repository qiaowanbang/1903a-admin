import { createApp } from "vue";
import App from "./App.vue";
import router from "@/router";
import antd from "@/plugin/antdUiplugin";
console.log(antd);
const $vm = createApp(App);
$vm.use(antd);
$vm.use(router);
$vm.mount("#app");
