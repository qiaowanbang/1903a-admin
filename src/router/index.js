import { createRouter, createWebHistory } from "vue-router";
const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/login",
      component: () => import("@/view/Login"),
    },
    {
      path: "/home",
      component: () => import("@/view/Home"),
      children: [
        {
          path: "/index",
          redirect: "/home/index",
        },
        {
          path: "/home/index",
          component: () => import("@/view/Home/Index/index"),
        },
        {
          path: "/home/produce",
          children: [
            {
              path: "/home/produce",
              redirect: "/home/produce/classify", // 重定向:重新指向其它path,会改变网址
            },
            {
              path: "/home/produce/classify",

              component: () => import("@/view/Home/Product/Classify"),
            },
            {
              path: "/home/produce/Comment",
              component: () => import("@/view/Home/Product/Comment"),
            },
            {
              path: "/home/produce/grouping",
              component: () => import("@/view/Home/Product/Grouping"),
            },
            {
              path: "/home/produce/product-S",
              component: () => import("@/view/Home/Product/Product-S"),
            },
            {
              path: "/home/produce/specification",
              component: () => import("@/view/Home/Product/Specification"),
            },
          ],
        },
        {
          path: "/home/shop",
          children: [
            {
              path: "/home/shop",
              redirect: "/home/shop/clasfreightsify", // 重定向:重新指向其它path,会改变网址
            },
            {
              path: "/home/shop/freight",
              component: () => import("@/view/Home/Shop/Freight"),
            },
            {
              path: "/home/shop/notice",
              component: () => import("@/view/Home/Shop/Notice"),
            },
            {
              path: "/home/shop/pickup",
              component: () => import("@/view/Home/Shop/Pick-up"),
            },
            {
              path: "/home/shop/search",
              component: () => import("@/view/Home/Shop/Search"),
            },
            {
              path: "/home/shop/swiper",
              component: () => import("@/view/Home/Shop/Swiper"),
            },
          ],
        },
        {
          path: "/home/system",
          children: [
            {
              path: "/home/system",
              redirect: "/home/shop/administrator", // 重定向:重新指向其它path,会改变网址
            },
            {
              path: "/home/system/administrator",
              component: () => import("@/view/Home/System/Administrator"),
            },
            {
              path: "/home/system/menu",
              component: () => import("@/view/Home/System/Menu"),
            },
            {
              path: "/home/system/parameter",
              component: () => import("@/view/Home/System/Parameter"),
            },
            {
              path: "/home/system/role",
              component: () => import("@/view/Home/System/Role"),
            },
            {
              path: "/home/system/site",
              component: () => import("@/view/Home/System/Site"),
            },
            {
              path: "/home/system/systemlog",
              component: () => import("@/view/Home/System/System-log"),
            },
            {
              path: "/home/system/timing",
              component: () => import("@/view/Home/System/Timing"),
            },
          ],
        },
        {
          path: "/home/vip",
          component: () => import("@/view/Home/Vip/Vips"),
        },
      ],
    },
  ],
});
export default router;
